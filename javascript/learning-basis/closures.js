for (var i = 0; i < 3; i++){
	  setTimeout(() => console.log(`With 'var i': ${i}`), 100)
}

for (var j = 0; j < 3; j++){
	  setTimeout(printFunction(`With 'printFunction(j)': ${j}`), 100)
}

for (let k = 0; k < 3; k++){
	  setTimeout(() => console.log(`With 'let k': ${k}`), 100)
}
  
function printFunction (val) {
	return function() {
		      console.log(val)
	}
}
