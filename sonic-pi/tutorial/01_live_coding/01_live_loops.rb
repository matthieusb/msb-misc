# Simple live loop
# Can be redefined on the fly

# :beats_choral here is the name

live_loop :beats_choral do
  sample :bd_haus
  sample :ambi_choir, rate: 0.1
  sample :ambi_glass_hum
  sleep 0.5 # Important element
end